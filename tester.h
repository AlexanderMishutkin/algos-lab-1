/**
 * В файле описан класс, проводящий тестирование алгоритмов
 */
#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-msc50-cpp"

#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include "iterable-sorts.h"
#include "linear-sorts.h"
#include "recursive-sorts.h"
#include <functional>
#include <chrono>
#include <fstream>

/**
 * Именованая ссылка на функцию сортировки
 */
using NamedSort = std::pair<std::string, std::function<void(std::vector<int> &)>>;

/**
 * Служебный класс, позволяющий измерять время работы программы в наносекундах с помощью chrono и high_resolution_clock
 */
struct Timer {
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    std::chrono::time_point<std::chrono::high_resolution_clock> pause_start;

    /**
     * Начинает отсчет вроемени с момента вызова
     */
    void Start() {
        start = std::chrono::high_resolution_clock::now();
    }

    /**
     * Приостанавливает отсчет вроемени
     */
    [[maybe_unused]] void Pause() {
        pause_start = std::chrono::high_resolution_clock::now();
    }

    /**
     * Продолжает отсчет вроемени с момента вызова Start
     */
    [[maybe_unused]] void Continue() {
        start += (pause_start - std::chrono::high_resolution_clock::now());
    }

    /**
     * Заканчивает отсчет врмени и возвращает результат
     * @return время в формате chrono
     */
    [[nodiscard]] auto Finish() const {
        return (std::chrono::high_resolution_clock::now() - start);
    }

    /**
     * Заканчивает отсчет врмени и возвращает результат
     * @return время в наносекундах
     */
    [[nodiscard]] long long FinishInNanoSec() const {
        return std::chrono::duration_cast<std::chrono::nanoseconds>(this->Finish()).count();
    }
};

/**
 * Класс, занимающийся генерацией массивов, вызовом сортировок и записью результатов в .csv файл
 */
class Tester {
    /**
     * Список функций сортировки и их имен
     */
    std::vector<NamedSort> _sorts;
    /**
     * Список типов заполнения
     */
    std::vector<std::string> _arrayFillTypes{"small values", "large values", "almost sorted", "decreasing"};
    Timer _timer;
    /**
     * Длины массивов
     */
    std::set<int> _lens;
    /**
     * Представление таблицы, из которого данные и попадают в файл
     */
    std::map<std::pair<int, int>, std::vector<long double>> _data_frame;

    /**
     * Создает вектор случайных чисел из диапазона
     * @param length длина вектора
     * @param min минимальное допустимое случайное число
     * @param max максимальное допустимое случайное число
     * @return вектор случайных чисел из диапазона
     */
    static std::vector<int> randomArray(int length, int min, int max) {
        std::vector<int> v(length);
        for (int i = 0; i < length; i++) {
            v[i] = rand() % (max - min) + min;
        }
        return v;
    }

    /**
     * Создает вектор подряд идущих по убыванию чисел
     * @param length длина массиива
     * @param min минимальное число в нем
     * @return вектор подряд идущих по убыванию чисел
     */
    static std::vector<int> decreasingArray(int length, int min) {
        std::vector<int> v(length);
        for (int i = 0; i < length; i++) {
            v[length - i - 1] = min++;
        }
        return v;
    }

    /**
     * Создает и сортирует случайный вектор
     * @param length длина вектора
     * @param min минимальное допустимое случайное число
     * @param max максимальное допустимое случайное число
     * @param decreasing True если вектор должен быть отсортирован по убыванию
     * @return сортированный вектор случайных чисел из диапазона
     */
    static std::vector<int> sortedArray(int length, int min, int max, bool decreasing = false) {
        std::vector<int> v = randomArray(length, min, max);
        // Кастомный компаратор ради возможности выбирать направление сортировки
        std::sort(v.begin(), v.end(), [decreasing](int a, int b) {
            if (!decreasing) return a <= b; else return b <= a;
        });
        return v;
    }

    /**
     * Создает и сортирует вектор случайных чисел, потом меняет местами несколько его элементов
     * @param length длина вектора
     * @param min минимальное допустимое случайное число
     * @param max максимальное допустимое случайное число
     * @return слегка недосортированный вектор случайных чисел из диапазона
     */
    static std::vector<int> distractedSortedArray(int length, int min, int max) {
        std::vector<int> v = sortedArray(length, min, max);
        int N = 8;
        for (int t = 0; t < length; t += 1000) {
            for (int cnt = 0; cnt < N; cnt++) {
                std::swap(v[rand() % (length - t) % 1000 + t], v[rand() % (length - t) % 1000 + t]);
            }
        }
        return v;
    }

    /**
     * Создает вектор по указанному типу распределения элементов
     * @param length длина вектора
     * @param flag тип распределения элементов
     * @return вектор по указанному типу распределения элементов
     */
    static std::vector<int> buildArray(int length, const std::string &flag) {
        if (flag == "small values") {
            return randomArray(length, 0, 6);
        }
        if (flag == "large values") {
            return randomArray(length, 0, 4001);
        }
        if (flag == "almost sorted") {
            return distractedSortedArray(length, 0, 4001);
        }
        if (flag == "decreasing") {
            return decreasingArray(length, 1);
        }
        throw std::invalid_argument("Unknown fill flag " + flag);
    }

    /**
     * Замеряет время работы сортировки на векторе
     * @param copy вектор который надо сортировать
     * @param sort сортировка
     * @param answer вектор для проверки корректности
     * @return время раюботы алгоритма сортировки в наносекундах (среднее)
     */
    long long applySortToArray(std::vector<int> copy, const NamedSort &sort, const std::vector<int> &answer) {
        _timer.Start();
        sort.second(copy);
        long long time = _timer.FinishInNanoSec();
        for (int i = 0; i < answer.size(); i++) {
            if (answer[i] != copy[i]) {
                throw std::invalid_argument("Sort \"" + sort.first + "\" is broken!");
            }
        }
        return time;
    }

    /**
     * Выполняет среднее время работы всех сортировок на векторе по нескольким проходам,
     * причем сначала идут пробные прогоны.
     * Результат записывает в _data_frame
     * @param length длина вектора
     * @param fillIndex тип заполнения вектора
     */
    void testOnLength(int length, int fillIndex) {
        std::vector<int> selection = buildArray(length, _arrayFillTypes[fillIndex]);
        std::vector<int> correct = std::vector<int>(selection.begin(), selection.end());
        std::sort(correct.begin(), correct.end());

        _lens.insert(length);
        // Вектор будут сортиоровать все имеющиеся сортировки
        for (int i = 0; i < _sorts.size(); i++) {
            long long warm_up = 5;
            long long accuracy = 20;
            long long sum = 0;

            // Пробный прогон, для оптимизации кэша
            for (int a = 0; a < warm_up; a++) {
                applySortToArray(selection, _sorts[i], correct);
            }

            // Прогон для замера среднего времени
            for (int a = 0; a < accuracy; a++) {
                sum += applySortToArray(selection, _sorts[i], correct);
            }

            long double avg = ((long double) sum) / (long double) accuracy;
            _data_frame[{i, fillIndex}].push_back(avg);
        }
    }

public:
    /**
     * Создает объект тестера по-умолчанию, то есть с данными восьмю сортировками
     */
    Tester() : _lens() {
        srand(time(nullptr) * 10);
        _sorts = std::vector<NamedSort>();
        _sorts.emplace_back("Bubble sort", bubbleSort);
        _sorts.emplace_back("Select sort", selectSort);
        _sorts.emplace_back("Binary sort", binaryInsertSort);
        _sorts.emplace_back("Count sort", countSort);
        _sorts.emplace_back("Digital sort", digitalSort);
        _sorts.emplace_back("Quick sort", quickSort);
        _sorts.emplace_back("Heap sort", heapSort);
        _sorts.emplace_back("Merge sort", mergeSort);
    }

    /**
     * Запускает замер среднего времени для каждого времени заполнения для каждой длины массива
     */
    void StartTesting() {
        for (int fillIndex = 0; fillIndex < _arrayFillTypes.size(); fillIndex++) {
            for (int length = 50; length <= 300; length += 50) {
                testOnLength(length, fillIndex);
            }
            for (int length = 1100; length <= 4100; length += 1000) {
                testOnLength(length, fillIndex);
            }
        }
    }

    /**
     * Записывает _data_frame в таблицу по адресу
     * @param path путь до таблицы
     */
    void DumpResults(const std::string &path) {
        std::fstream out(path, std::ios::out);

        out << "Array size";
        for (const auto &p : _data_frame) {
            out << ";" << _sorts[p.first.first].first << " - " << _arrayFillTypes[p.first.second];
        }
        out << std::endl;

        int i = 0;
        for (auto len : _lens) {
            out << len;
            for (auto p : _data_frame) {
                out << ";" << p.second[i];
            }
            out << std::endl;
            i++;
        }

        out.close();
    }
};
