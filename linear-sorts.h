/**
 * В данном файле собраны линейные сортировки
 */
#include <vector>
#include <cstdint>

/**
 * Сортировка подсчетом
 * @param v неупорядоченный вектор
 */
void countSort(std::vector<int> &v) {
    int n = v.size();
    if (n <= 1) return;
    int min, max;
    min = max = v[0];

    for (int i = 1; i < n; i++) {
        if (v[i] < min) {
            min = v[i];
        }
        if (v[i] > max) {
            max = v[i];
        }
    }

    int *cnt = new int[max - min + 1];
    for (int i = 0; i < max - min + 1; i++) {
        cnt[i] = 0;
    }
    for (int i = 0; i < n; i++) {
        cnt[v[i] - min]++;
    }
    for (int i = 0; i < max - min; i++) {
        cnt[i + 1] += cnt[i];
    }

    int *tmp = new int[n];
    for (int i = n - 1; i >= 0; i--) {
        tmp[--cnt[v[i] - min]] = v[i];
    }
    delete[] cnt;
    for (int i = 0; i < n; i++) {
        v[i] = tmp[i];
    }
    delete[] tmp;
}

/**
 * Служебный тип для удобной работы с 256ричным представлением числа
 */
union Base_256t {
    std::uint32_t num;
    std::uint8_t word[4];
};

/**
 * Цифровая сортировка (по основанию 256)
 * @param v неупорядоченный вектор
 */
void digitalSort(std::vector<int> &vec) {
    int n = vec.size();
    if (n <= 1) return;
    auto *v_c = reinterpret_cast<Base_256t *>(vec.data());
    auto *v = new Base_256t[n];
    for (int i = 0; i < n; i++) {
        v[i] = v_c[i];
    }

    auto *tmp = new Base_256t[n];
    int *cnt = new int[256];

    for (int word_n = 0; word_n < 4; word_n++) {
        for (int i = 0; i < 256; i++) {
            cnt[i] = 0;
        }

        for (int i = 0; i < n; i++) {
            cnt[v[i].word[word_n]]++;
        }

        for (int i = 0; i < 255; i++) {
            cnt[i + 1] += cnt[i];
        }

        for (int i = n - 1; i >= 0; i--) {
            tmp[--cnt[v[i].word[word_n]]] = Base_256t{v[i].num};
        }

        Base_256t *t = v;
        v = tmp;
        tmp = t;
    }

    delete[] cnt;
    delete[] tmp;
    auto *v_i = reinterpret_cast<int *>(v);
    for (int i = 0; i < n; i++) {
        vec[i] = v_i[i];
    }
    // Удаления проверены с помощью санитайзера, все нормально
    delete[] v;
}
