/**
 * В данном файле собраны рекурсивные сортировки
 */
#include <vector>
#include <iostream>

/**
 * Быстрая сортировка
 * @param v неупорядоченный вектор
 * @param l граница участка, сортируемого сейчас
 * @param r граница участка, сортируемого сейчас
 */
void quickSortIteration(std::vector<int> &v, int l, int r) {
    if (r == -1) {
        r = (int) v.size() - 1;
    }

    int i = l;
    int j = r;
    int m = (i + j) / 2;
    int x = v[m];

    while (i <= j) {
        while (v[i] < x) {
            ++i;
        }
        while (v[j] > x) {
            --j;
        }

        if (i <= j) {
            std::swap(v[i++], v[j--]);
        }
    }

    if (l < j) {
        quickSortIteration(v, l, j);
    }
    if (r > i) {
        quickSortIteration(v, i, r);
    }
}

/**
 * Быстрая сортировка. Это лишь обертка предыдущей функции,
 * позволяющая использовать ее с сигнатурой, схожей с остальными сортировками
 * @param v неупорядоченный вектор
 */
void quickSort(std::vector<int> &v) {
    quickSortIteration(v, 0, v.size() - 1);
}

/**
 * Исправляет узел дерева, чтобы оно осталось кучей
 * @param root узел для исправления
 * @param v вектор с деревом
 * @param n длина вектора v
 */
void heapify(int root, std::vector<int> &v, int n) {
    int right = root * 2 + 2;
    int left = root * 2 + 1;
    if (left >= n) return;
    if (right >= n) {
        if (v[left] > v[root]) {
            std::swap(v[left], v[root]);
        }
        return;
    }
    if (v[left] >= v[right] && v[left] > v[root]) {
        std::swap(v[left], v[root]);
        heapify(left, v, n);
        return;
    }
    if (v[right] >= v[left] && v[right] > v[root]) {
        std::swap(v[right], v[root]);
        heapify(right, v, n);
        return;
    }
}

/**
 * Превращает вектор в кучу, записывает кучу в этот же вектор
 * @param v вектор чисел
 * @param n длина вектора v
 */
void buildHeap(std::vector<int> &v, int n) {
    for (int i = n - 1; i >= 0; i--) {
        heapify(i, v, n);
    }
}

/**
 * Сортирует вектор строя на нем кучу, а затем забирая из нее элементы по-одному
 * @param v неупорядоченный вектор
 */
void heapSort(std::vector<int> &v) {
    int n = v.size();
    buildHeap(v, v.size());
    for (int i = n - 1; i >= 0; i--) {
        std::swap(v[0], v[i]);
        heapify(0, v, i);
    }
}

/**
 * Сортировка слиянием
 * @param v неупорядоченный вектор
 * @param l граница участка, сортируемого сейчас
 * @param r граница участка, сортируемого сейчас
 * @param buf дополнительный вектор, чтобы не создавать новый при необходимости записать результат слияния
 */
void mergeSortIteration(std::vector<int> &v, int l, int r, std::vector<int> &buf) {
    if (l == r - 1) {
        return;
    }

    int m = (l + r) / 2;
    mergeSortIteration(v, l, m, buf);
    mergeSortIteration(v, m, r, buf);

    int x, y;
    x = l;
    y = m;
    int i = l;

    while (x < m && y < r) {
        if (v[x] <= v[y]) {
            buf[i++] = v[x++];
        } else {
            buf[i++] = v[y++];
        }
    }

    while (x < m) {
        buf[i++] = v[x++];
    }

    while (y < r) {
        buf[i++] = v[y++];
    }

    for (i = l; i < r; i++) {
        v[i] = buf[i];
    }
}

/**
 * Сортировка слиянием. Обертка со стандартной сигнатурой
 * @param v неупорядоченный вектор
 */
void mergeSort(std::vector<int> &v) {
    std::vector<int> buf(v.size());
    mergeSortIteration(v, 0, v.size(), buf);
}
