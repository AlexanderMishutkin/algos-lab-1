/**
 * Точка входа проекта, по умолчанию просто выполняет измерения
 */
#include "tester.h"
#include <iostream>

int main() {
    Tester tester;
    tester.StartTesting();
    // Так себе название, но почему бы и нет
    tester.DumpResults("csv.csv");
}
