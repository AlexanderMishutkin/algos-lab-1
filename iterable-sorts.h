/**
 * В данном файле собраны итерационные сортировки
 */
#include <vector>

/**
 * Сортирует вектор пузырьком
 * @param v неупорядоченный вектор
 */
void bubbleSort(std::vector<int> &v) {
    int n = v.size();

    for (int j = 0; j < n; j++) {
        for (int i = 0; i < n - 1; i++) {
            if (v[i] > v[i + 1]) {
                std::swap(v[i], v[i + 1]);
            }
        }
    }
}

/**
 * Сортирует вектор выбором
 * @param v неупорядоченный вектор
 */
void selectSort(std::vector<int> &v) {
    int ind, min;
    int n = v.size();

    for (int j = 0; j < n - 1; j++) {
        ind = j;
        min = v[j];

        for (int i = j; i < n; i++) {
            if (v[i] < min) {
                min = v[i];
                ind = i;
            }
        }

        if (ind != j) {
            std::swap(v[j], v[ind]);
        }
    }
}

/**
 * Служебная функция находящая индекс последнего элемента меньше или равного данного
 * @param v вектор для поиска, интервад (l, r) на нем должен быть отсортирован
 * @param l левая граница интервала
 * @param r правая граница интервала
 * @param x искомый элемент
 * @return индекс последнего элемента меньше или равного данного
 */
int upperBound(std::vector<int> &v, int l, int r, int x) {
    int m = (l + r) / 2;
    if (m == l) {
        if (v[m] > x) {
            return m;
        } else {
            return m + 1;
        }
    }

    if (v[m] > x) {
        return upperBound(v, l, m, x);
    } else {
        return upperBound(v, m, r, x);
    }
}

/**
 * Сортирует вектор вставками с бинарным поиском позиции
 * @param v неупорядоченный вектор
 */
void binaryInsertSort(std::vector<int> &v) {
    int n = v.size();

    for (int j = 1; j < n; j++) {
        int ind = upperBound(v, 0, j, v[j]);
        int tmp = v[j];
        for (int i = j - 1; i >= ind; i--) {
            v[i + 1] = v[i];
        }
        v[ind] = tmp;
    }
}
